//
//  ELIImageOfGalery.h
//  Kogi_Feed_Example_1
//
//  Created by Adriana on 31/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELIImageOfGalery : NSObject

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSString *caption;

@end
