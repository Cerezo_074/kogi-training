//
//  ELIViewController.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 4/29/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELIViewController.h"
#import "ELIFormRegister.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "CustomUser.h"
#import "ELIGalery.h"
#import "ELIImageOfGalery.h"
#import "ELIViewControllerForLaunchWhenClickOnWebview.h"
#import "ContentViewController.h"
#import <AFNetworkReachabilityManager.h>

#define keyForCaseInSensitive @"ForCaseInSensitive_Key"
#define keyForAllText @"ForAllText_key"

@interface ELIViewController ()<UIWebViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate>

@property (nonatomic) float mark;
@property (nonatomic) float space;
@property (nonatomic) float sizeOfScroll;
@property (nonatomic, strong) NSArray *galleries;

@end

@implementation ELIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithItem:(RSSItem *)item index:(NSInteger)index;
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        self.item = item;
        self.space = 10;
        self.mark = 0;
        self.sizeOfScroll = 0;
        self.index = index;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSString *body = self.item.description;
    //NSLog(@"\n\n++++++++++++++++++++++++++++++++++++\n%@\n\n+++++++++++++++++++++++++++++++++\n\n",body);
    NSArray *extractTheTags = [self extractTheGalleriesonTetx:body];
    NSString *newBody = nil;
    
    if ([extractTheTags count]>1) {
        
        NSString *newBodyWithOutLink = [[self extractTheGalleriesonTetx:body] lastObject];
        newBody = [newBodyWithOutLink stringByAppendingString:[self insertLinkForTheGalleries:newBodyWithOutLink urlFromTheGalleries:[extractTheTags firstObject]]];
        NSMutableArray *tempMutableArray = [extractTheTags mutableCopy];
        [tempMutableArray removeLastObject];
        self.galleries = [NSArray arrayWithArray:tempMutableArray];
    }
    else
    {
        newBody = [extractTheTags lastObject];
    }
    
    NSString *styleCSS = @"<style>p{max-width: 280px;}figure{margin-left: 5px;}*{word-wrap: break-word;}</style>";
    NSString *javascript = @"<script type = \"text/javascript\"> window.onload = function(){ window.location.href= \"trickforheight://\"+document.body.offsetHeight; }</script>";
    NSString *tempHtml = @"<!DOCTYPE html><html><head>%@%@</head><body>%@<br></body></html>";
    NSString *html = [NSString stringWithFormat:tempHtml, styleCSS, javascript, newBody];
    
    //NSLog(@"\n\n---------------------------------------\n%@\n\n--------------------------------------\n\n",html);
    //Adjust the intials the origin point on y of the UILabel and scroll
    self.mark = self.space;
    self.sizeOfScroll = self.scroll.frame.size.width;
    
    
    //Adjust the label title
    NSString *title = self.item.title;
    UIFont *fontForTitle = [self.titleOfTheNotice font];
    CGRect sizeForTitle = [title boundingRectWithSize:CGSizeMake(280, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:fontForTitle}
                                              context:nil];
    self.titleOfTheNotice.text = title;
    self.titleOfTheNotice.frame = CGRectMake(0, self.mark, self.sizeOfScroll, sizeForTitle.size.height);
    
    //Adjust the mark for the origin point on y of the UIImageview
    self.mark = (self.space+sizeForTitle.size.height)+self.space;
    
    //Adjust the Imageview Image
    UIImage *image = [UIImage imageNamed:@"google.png"];
    self.ImageOfTheNotice.image = nil;
    [self.ImageOfTheNotice setImageWithURL:self.item.imageUrl placeholderImage:image];
    self.ImageOfTheNotice.frame = CGRectMake(0, self.mark, self.sizeOfScroll, self.sizeOfScroll);
    
    //Adjust the mark for the origin point on y of the UIwebview
    self.mark  = (self.mark+self.sizeOfScroll+self.space);
    
    //Adjust the UIwebView
    self.bodyOfTheNotice.frame = CGRectMake(0, self.mark, self.sizeOfScroll, self.sizeOfScroll);
    [self.bodyOfTheNotice loadHTMLString:html baseURL:nil];
    self.bodyOfTheNotice.delegate = self;
    self.bodyOfTheNotice.scrollView.scrollEnabled = NO;
    self.bodyOfTheNotice.frame = CGRectMake(0, self.mark, 280, 10);
    self.scroll.contentSize = CGSizeMake(self.sizeOfScroll, self.mark+ 10);
    
}

#pragma mark Delegate Methods for UIWbeView

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{

    if (navigationType == UIWebViewNavigationTypeLinkClicked) {

        if ([[request.URL description] isEqualToString:@"http://eli.pruebas.com/"]) {
            
            //[[[UIAlertView alloc]initWithTitle:@"Please Come Back to Later" message:@"Missing Implementation" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil]show];
            
            ContentViewController *controller = [[ContentViewController alloc]initWithArrayOfGalleries:self.galleries];
            [self.navigationController pushViewController:controller animated:YES];
            
            return NO;
            
        }
        else
        {
            
            [self.navigationController pushViewController:[[ELIViewControllerForLaunchWhenClickOnWebview alloc]initWithUrl:request.URL] animated:YES];
            return NO;
            
        }
        
    }
    
    NSURL *url = [request URL];
    if (navigationType == UIWebViewNavigationTypeOther) {
        if ([[url scheme] isEqualToString:@"trickforheight"]) {
            float height = [[url host]floatValue];
            webView.frame = CGRectMake(0, self.mark, 280, height);
            self.mark = (self.mark +height)+self.space+15;
            self.scroll.contentSize = CGSizeMake(280, self.mark);
        }
    }

    return YES;
    
}

#pragma mark Methods for the Regular Expressions

-(NSArray*)extractTheGalleriesonTetx:(NSString*)string{
    
    NSError *error = nil;
    NSMutableArray *galleries = [[NSMutableArray alloc]init];
    NSString *patter = @"<mir:image_gallery .*>(.|\n)*?</mir:image_gallery>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:patter
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];

    NSArray *temp =  [regex matchesInString:string
                                    options:0
                                      range:NSMakeRange(0, [string length])];
    
    if ([temp count] == 0) {
        [galleries addObject:string];
        return galleries;
    }
    
    for (NSTextCheckingResult *textResult in temp) {
        
        NSString *string2 = [string substringWithRange:[textResult range]];
        
        //Extract the image_gallery tag
        NSString *patterForTitleOfGallery = @"<mir:image_gallery title.*>";
        ELIGalery *galery = [[ELIGalery alloc]init];
        regex = nil;
        regex = [NSRegularExpression regularExpressionWithPattern:patterForTitleOfGallery
                                                          options:0
                                                            error:&error];
        
        NSInteger matchForTitle = [regex numberOfMatchesInString:string2
                                                         options:0
                                                           range:NSMakeRange(0, [string2 length])];
        
        if (matchForTitle == 1) {
            
            NSArray *tempForTitle = [regex matchesInString:string2
                                                   options:0
                                                     range:NSMakeRange(0, [string2 length])];
            
            NSMutableString *stringForTitle = [[string2 substringWithRange:[(NSTextCheckingResult *) [tempForTitle firstObject] range]]mutableCopy];
            [stringForTitle replaceOccurrencesOfString:@"<mir:image_gallery title=\"" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [stringForTitle length])];
            
            [stringForTitle replaceOccurrencesOfString:@"\">" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [stringForTitle length])];
            galery.title = stringForTitle;
            
        }
        else
        {
            galery.title = @"Custom Gallery";
        }
        
        //Extract the image tags
        NSString *patter2 = @"<mir:image>(\n|.)*?</mir:image>";
        regex = nil;
        regex = [NSRegularExpression regularExpressionWithPattern:patter2
              
                                                          options:NSRegularExpressionCaseInsensitive error:&error];
        NSArray *temp2 = [regex matchesInString:string2
                                        options:0
                                          range:NSMakeRange(0, [string2 length])];
        
        //Array for content the ELIImageOfGalery objects
        NSMutableArray *arrayOfImages = [[NSMutableArray alloc]init];
        for (int i = 0;i < [temp2 count]; i++) {
            
            NSTextCheckingResult *temp3 = temp2 [i];
            NSMutableString * tagImageComplete = [[string2 substringWithRange:[temp3 range]] mutableCopy];
            ELIImageOfGalery *image = [[ELIImageOfGalery alloc]init];
            
            //Extract the URL tag
            NSString *patterForUrl = @"<mir:url>(\n|.)*?</mir:url>";
            regex = nil;
            regex = [NSRegularExpression regularExpressionWithPattern:patterForUrl
                                                              options:0
                                                                error:&error];
            
            NSUInteger matchForURL = [regex numberOfMatchesInString:tagImageComplete
                                                            options:0
                                                              range:NSMakeRange(0, [tagImageComplete length])];
            
            if (matchForURL == 1) {
                
                NSArray *tempForURLS = [regex matchesInString:tagImageComplete options:0
                                                        range:NSMakeRange(0, [tagImageComplete
                                                                              length])];
                NSTextCheckingResult *resultOfTheUrl = [tempForURLS firstObject];
                NSMutableString *stringURL = [[tagImageComplete substringWithRange:[resultOfTheUrl range]]mutableCopy];
                
                [stringURL replaceOccurrencesOfString:@"<mir:url>"
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [stringURL length])];
                
                [stringURL replaceOccurrencesOfString:@"</mir:url>"
                                           withString:@""
                                              options:NSLiteralSearch
                                                range:NSMakeRange(0, [stringURL length])];
                
                image.url = [NSURL URLWithString:stringURL];
                
            }
            
            //Extract the Caption tag
            NSString *patterFotCaption = @"<mir:caption>(\n|.)*?</mir:caption>";
            regex = nil;
            regex = [NSRegularExpression regularExpressionWithPattern:patterFotCaption
                                                              options:0
                                                                error:&error];
            
            NSUInteger matchForCaption = [regex numberOfMatchesInString:tagImageComplete
                                                                options:0
                                                                  range:NSMakeRange(0, [tagImageComplete length])];
            
            if (matchForCaption == 1) {
                
                NSArray *tempForCaptions = [regex matchesInString:tagImageComplete options:0 range:NSMakeRange(0, [tagImageComplete length])];
                NSTextCheckingResult *resultOfTheCaption = [tempForCaptions firstObject];
                NSMutableString *stringCaption = [[tagImageComplete substringWithRange:[resultOfTheCaption range]] mutableCopy];
                
                [stringCaption replaceOccurrencesOfString:@"<mir:caption>"
                                               withString:@""
                                                  options:NSLiteralSearch
                                                    range:NSMakeRange(0, [stringCaption length])];
                
                [stringCaption replaceOccurrencesOfString:@"</mir:caption>"
                                               withString:@""
                                                  options:NSLiteralSearch
                                                    range:NSMakeRange(0, [stringCaption length])];
                image.caption = stringCaption;
                
            }
            
            [arrayOfImages addObject:image];
            
        }
        
        galery.gallery = arrayOfImages;
        [galleries addObject:galery];
        
    }
    
    
    NSRegularExpression *regexExpression = [[NSRegularExpression alloc]initWithPattern:@"<mir:image_gallery .*>(.|\n)*?</mir:image_gallery>"
                                                                       options:0
                                                                                error:&error];
    
    NSString *stringWithoutText = [regexExpression stringByReplacingMatchesInString:string options:0
                                                                              range:NSMakeRange(0, [string length])
                                                                       withTemplate:@""];
    
    regexExpression = [[NSRegularExpression alloc]initWithPattern:@"<p>[\n|.|\\s]+</p>"
                                                          options:0
                                                            error:&error];
    
    NSString *stringWithoutPTags = [regexExpression stringByReplacingMatchesInString:stringWithoutText options:0
                                                                              range:NSMakeRange(0, [stringWithoutText length])
                                                                       withTemplate:@""];
    
    [galleries addObject:stringWithoutPTags];
    
    return galleries;
    
}

#pragma mark Utils Methods

-(NSString*)insertLinkForTheGalleries:(NSString*)text urlFromTheGalleries:(ELIGalery*)gallery{

    if (!gallery) {
        return text;
    }
    
    ELIImageOfGalery *tempImage = [gallery.gallery firstObject];
    NSString *url = [tempImage.url description];
    NSString *caption = @"See More...";
    NSString *height = [NSString stringWithFormat:@"%.2f", (self.scroll.frame.size.width - 20)];
    NSString *temp = @"<p><a href = \"http://eli.pruebas.com\"><figure><img src =\"%@\" width=\"%@\" height=\"%@\"/><figcaption><strong>%@</strong></figcaption></figure></a></p>";
    NSString *elemtenForHtml = [NSString stringWithFormat:temp,url,height,height,caption];
    
    return elemtenForHtml;
    
}

@end