//
//  ELIFormRegister.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 1/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SBFormViewController.h>
#import "ELIRegisterProtocol.h"
#import <AVFoundation/AVFoundation.h>

@interface ELIFormRegister : SBFormViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblBirthDay;
@property (weak, nonatomic) IBOutlet UIDatePicker *pickDateOfBirth;
@property (weak, nonatomic) IBOutlet UIButton *accept;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageAnimation;
@property (weak, nonatomic) IBOutlet UIImageView *globe;
@property (weak, nonatomic) id<ELIRegisterProtocol> delegate;

@end
