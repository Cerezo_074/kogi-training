//
//  ELICustomAlertViewViewController.m
//  Kogi_Feed_Example_1
//
//  Created by Adriana on 24/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELICustomAlertViewViewController.h"
#import "CustomUser.h"
#import "PageViewControllerForRSSItems.h"

@interface ELICustomAlertViewViewController () <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning, UITextFieldDelegate>

@end

@implementation ELICustomAlertViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    //Adjust the border for the view
    self.alertView.layer.borderColor = [UIColor blueColor].CGColor;
    self.alertView.layer.borderWidth = 4;
    self.alertView.layer.cornerRadius = 10;
    
    //set the txtUserName for to be the firstResponder
    [self.txtUserName becomeFirstResponder];
    self.txtUserName.delegate = self;
    self.txtPassword.delegate = self;

}



#pragma mark Target Action Buttons

- (IBAction)acceptButton:(id)sender {

    PageViewControllerForRSSItems *referenceForPresentingViewController = [[(UINavigationController*) self.presentingViewController viewControllers]lastObject];
    [referenceForPresentingViewController startTheProcessForLogInWithUserName:self.txtUserName.text password:self.txtPassword.text];
    [self.presentingViewController dismissViewControllerAnimated:YES
                                                      completion:nil];
}

- (IBAction)cancelButton:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Utils Methods

-(void)userLogInSuccessfully
{
    [self eraseAllTheFields];
    [self sendMessageToUser:@"The User was Autentified" title:@"User Created"];
}

-(void)eraseAllTheFields
{
    self.txtPassword.text = @"";
    self.txtUserName.text = @"";
}

-(void)sendMessageToUser:(NSString*)message title:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                   }
                   );
}

#pragma mark Methods for the UIViewControllerTransitioningDelegate delegate

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                 presentingController:(UIViewController *)presenting
                                                                    sourceController:(UIViewController *)source {
    return self;
    
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{

    return self;
    
}

#pragma mark Methods for the UIViewControllerAnimatedTransitioning delegate

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{

    return 0.50;
    
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{

    UIViewController* vc1 =[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* vc2 =[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView* con = [transitionContext containerView];
    UIView* v1 = vc1.view;
    UIView* v2 = vc2.view;
    
    if (vc2 == self) {
        // presenting
        [con addSubview:v2];
        v2.frame = v1.frame;
        self.alertView.transform = CGAffineTransformMakeScale(1.6,1.6);
        v2.alpha = 0;
        v1.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
        [UIView animateWithDuration:0.50 animations:^{
            v2.alpha = 1;
            self.alertView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    }
    else
    {
        // dismissing
        [UIView animateWithDuration:0.50 animations:^{
            self.alertView.transform = CGAffineTransformMakeScale(0.5,0.5);
            v1.alpha = 0;
        } completion:^(BOOL finished) {
            v2.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
            [transitionContext completeTransition:YES];
        }];
    }
}

#pragma mark Methods of the Delegate for UItextField

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if ([textField isEqual:self.txtPassword]) {
        PageViewControllerForRSSItems *referenceForPresentingViewController = [[(UINavigationController*) self.presentingViewController viewControllers]lastObject];
        [referenceForPresentingViewController startTheProcessForLogInWithUserName:self.txtUserName.text password:self.txtPassword.text];
        [self.presentingViewController dismissViewControllerAnimated:YES
                                                          completion:nil];
    }
    
    [textField resignFirstResponder];
    return YES;
    
}

@end
