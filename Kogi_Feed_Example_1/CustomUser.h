//
//  CustomUser.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 5/15/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#define userPath @"user.obj"

#import <Foundation/Foundation.h>

@interface CustomUser : NSObject <NSCoding>

@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* phoneNumber;
@property (nonatomic, strong) NSDate* birthDay;

+(CustomUser*)loadUserFromDiskWithPath:(NSString*)userStoragePath;

+(BOOL)saveUserToDisk:(CustomUser*)user userStoragePath:(NSString*)userStoragePath;

+(BOOL)deleteUserFromDiskAndMemory;

+(CustomUser*)currentUser;

+(void)setCurrentUser:(CustomUser*)user;

+(void)createRemoteUser:(CustomUser*)user onCompletion:(void(^)(BOOL state, id error))block;

+(void)logInUser:(CustomUser*)user onCompletion:(void(^)(BOOL state, id error))block;

-(BOOL)saveUserState;

@end
