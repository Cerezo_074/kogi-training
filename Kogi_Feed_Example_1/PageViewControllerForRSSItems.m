//
//  PageViewControllerForRSSItems.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 23/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "PageViewControllerForRSSItems.h"
#import "ELIViewController.h"
#import "ELIFormRegister.h"
#import "ELICustomAlertViewViewController.h"
#import "ELIRegisterProtocol.h"

@interface PageViewControllerForRSSItems ()<UIPageViewControllerDataSource, UIGestureRecognizerDelegate, UIActionSheetDelegate, ELIRegisterProtocol, UITextFieldDelegate>

@property(nonatomic) NSInteger startIndex;
@property(nonatomic, strong) UIAlertView *alert;

@end

@implementation PageViewControllerForRSSItems

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithModel:(NSArray*)model IndexForCurrentRSSItem:(NSInteger)index{

    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.model = model;
        self.startIndex = index;
    }
    
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //instace the pageViewController
    self.pageController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                                      navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                                    options:nil];
    //asign the dataSource
    self.pageController.dataSource = self;
    
    //Extract the frame of the view property for after asign it at the pagecontroller
    self.pageController.view.frame = self.tempView.frame;
    
    //Remove the view from this controller
    [self.tempView removeFromSuperview];
    
    //create the initial viewcontroller
    ELIViewController *initialController = [[ELIViewController alloc]initWithItem:[self.model objectAtIndex:self.startIndex] index:self.startIndex];
    //create the array whit the first object and passing this array for the pageController
    NSArray *viewControllers = @[initialController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    //Adding the pageController Property at this controller and move the parent controller at this controller
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    //Instance the Button for logIn or Sign up
    self.userButton = [[UIBarButtonItem alloc]initWithTitle:nil
                                                      style:UIBarButtonItemStylePlain
                                                     target:self
                       
                                                     action:@selector(startActionForBarButton:)];
    self.userButton.tintColor = [UIColor purpleColor];
    self.navigationItem.rightBarButtonItem = self.userButton;

}

-(void)viewWillAppear:(BOOL)animated{

    [self updateCurrentUserStateOnCorner];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    //Disable the back swipe gesture for the NavigationController
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    //Enable the back swipe gesture for the NavigationController
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]){
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
        
    }
    
}

#pragma mark Utils Methods

-(ELIViewController*)viewControllerAtIndex:(NSUInteger)index{
    
    return [[ELIViewController alloc]initWithItem:[self.model objectAtIndex:index] index:index];
    
}

-(void)sendMessageToUser:(NSString*)message title:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                   });
}

-(void)startActionForBarButton:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc]initWithTitle:@"Select an Option for User's Account" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Sign In", @"Register", @"Log Out", nil];
    [popup showInView:self.view];
}

-(void)userLogInSuccessfully
{
    [self sendMessageToUser:@"The User was Autentified" title:@"User Created"];
    [self updateCurrentUserStateOnCorner];
    if (self.alert) {
        [self.alert dismissWithClickedButtonIndex:0 animated:YES];
    }
}

-(void)updateCurrentUserStateOnCorner
{
    //Adjust the user
    self.user = [CustomUser currentUser];
    
    //Adjust the UIBarButton Item
    self.userButton.title = (self.user.username) ? self.user.username:@"Log In";
    
}

- (void)startTheProcessForLogInWithUserName:(NSString*)username password:(NSString*)password {
    
    if ([username length] > 0 && [password length] > 0)
    {
        CustomUser *temp = [[CustomUser alloc]init];
        temp.username = username;
        temp.password = password;
        //Block
        [CustomUser logInUser:temp onCompletion:^(BOOL state , id error) {
            if (state) {
                [self userLogInSuccessfully];
                /*
                 Because we have two forms for the Log In, we must be sure wich form is on this device.
                 The First is for ios 6 or less, that consist on an alerview with style for log in, this
                 form not needed to be presented for an controller, we have an reference for the alerview.
                 The second form is presenting a view controller. On this solution we need chechk if the 
                 property for the alertview is using, if the property isn't using we call at presenting 
                 controller for dismiss the presented view controller because we did to use the present view
                 controller technique.
                 */
                if (!self.alert) {
                    [self updateCurrentUserStateOnCorner];
                }
            }
            else
            {
                if ([error isKindOfClass:[NSError class]]) {
                    [self sendMessageToUser:[error localizedDescription] title:@"Error"];
                }
                else
                {
                    [self sendMessageToUser:error title:@"Error"];
                }
            }
        }];
        
    }
    else
    {
        [self sendMessageToUser:@"Error the Fields must be filled, check these please" title:@"Error"];
    }
    
}

#pragma mark Data Source Methods for the PageViewController

//Backward
-(UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    
    ELIViewController *currentController = (ELIViewController*)viewController;
    NSInteger currentIndex = currentController.index;
    
    if (currentIndex == 0) {
        return nil;
    }
    
    currentIndex--;
    ELIViewController *newController = [self viewControllerAtIndex:currentIndex];
    
    return newController;
}

//Forward
-(UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    
    ELIViewController *currentController = (ELIViewController*)viewController;
    NSInteger currentIndex = currentController.index;
    
    if (currentIndex == 24) {
        return nil;
    }
    
    currentIndex++;
    ELIViewController *newController = [self viewControllerAtIndex:currentIndex];
    
    return newController;

}

#pragma mark Delegate Methods for UIAlertView

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //This AlertView is For the Show a message for the User
    if([alertView.title isEqualToString:@"Log Out"]){
        
        if (buttonIndex == 1) {
            if ([CustomUser deleteUserFromDiskAndMemory]) {
                [self sendMessageToUser:@"The Session was closed" title:@"Log Out was Completed"];
                self.userButton.title = @"Log In";
            }
            else
            {
                [self sendMessageToUser:@"Error, please consult support, AXS123" title:@"Error"];
            }
        }
    }
    else
    {
        //When the AlertView is for Log In
        if (buttonIndex == 1) {
            //When the User tap the UIAlertView on Accept

            UITextField *tempUserName = [alertView textFieldAtIndex:0];
            UITextField *tempPasword = [alertView textFieldAtIndex:1];
            [self startTheProcessForLogInWithUserName:tempUserName.text password:tempPasword.text];
    
        }
    }
}

#pragma mark Delegate Methods for UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            //sign up
            if ([[CustomUser currentUser]username] != nil ) {
                [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
                [self sendMessageToUser:@"Close the session first please" title:@"Error"];
                return;
            }
            
            NSString *versionOfTheIOS = [[UIDevice currentDevice] systemVersion];
            NSInteger ver = [versionOfTheIOS integerValue];
            
            //On IOS 6 or less
            if (ver < 7) {
             
                self.alert = [[UIAlertView alloc]initWithTitle:@"Log In" message:@"Please enter your Username and Password" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Accept", nil];
                self.alert.title = @"Log In";
                self.alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
                UITextField *username = [self.alert textFieldAtIndex:0];
                UITextField *password = [self.alert textFieldAtIndex:1];
                password.tag = 1;
                [username setReturnKeyType:UIReturnKeyDone];
                [password setReturnKeyType:UIReturnKeyDone];
                username.delegate = self;
                password.delegate = self;
                [self.alert show];
                
            }
            //IOS 7 or later
            else
            {
                ELICustomAlertViewViewController *alertViewController = [[ELICustomAlertViewViewController alloc]initWithNibName:@"ELICustomAlertViewViewController"
                                                                                                                          bundle:nil];
                [self presentViewController:alertViewController animated:YES completion:nil];
            }
            
            break;
        }
        case 1:
        {
            //register
            if ([[CustomUser currentUser]username ]!= nil) {
                [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
                [self sendMessageToUser:@"Close the session first please" title:@"Error"];
                return;
            }
            ELIFormRegister *formToRegister = [[ELIFormRegister alloc]init];
            formToRegister.delegate = self;
            UINavigationController *controller = [[UINavigationController alloc]initWithRootViewController:formToRegister];
            [self presentViewController:controller animated:YES completion:nil];
            break;
        }
        case 2:
        {
            //log out
            if ([[CustomUser currentUser]username] == nil) {
                [self sendMessageToUser:@"The user don't exist, please log in before" title:@"Error"];
                return;
            }
            [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
            [[[UIAlertView alloc]initWithTitle:@"Log Out" message:@"Press Yes for close the session or not to Cancel" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil] show];
            break;
        }
        default:
            NSLog(@"Cancel Button");
            break;
    }
    
}

#pragma mark Methods of the Delegate for ELIFormSignUp

-(void)userHasDecidedToBackward:(id)presentingController{

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark Methods of the Delegate for ELIFormSignUp

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    if (textField.tag == 1) {
        
        if (self.alert) {

            UITextField *temp = [self.alert textFieldAtIndex:0];
            NSString *username = temp.text;
            NSString *password = textField.text;
            [self startTheProcessForLogInWithUserName:username password:password];            
        }
        
    }
    
    [textField resignFirstResponder];
    return YES;
    
}

@end
