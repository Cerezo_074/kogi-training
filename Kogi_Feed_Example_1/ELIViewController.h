//
//  ELIViewController.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 4/29/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSItem.h"

@interface ELIViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleOfTheNotice;
@property (weak, nonatomic) IBOutlet UIImageView *ImageOfTheNotice;
@property (weak, nonatomic) IBOutlet UIWebView *bodyOfTheNotice;
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (strong, nonatomic) RSSItem * item;
@property (nonatomic) NSInteger index;

-(id)initWithItem:(RSSItem *)item index:(NSInteger)index;

@end
