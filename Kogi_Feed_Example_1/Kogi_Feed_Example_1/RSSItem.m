//
//  RSSItem.m
//  Kogi_Feed_Example_1
//
//  Created by Ernesto Carrion on 5/13/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "RSSItem.h"
#import <AFNetworking.h>
#import <DDXMLDocument.h>
#import <DDXMLElement.h>
#import "ELIRequestOperationManager.h"

#define titleKey @"keyTitle"
#define linkKey @"keyLink"
#define descriptionKey @"keyDescription"
#define thumbnailUrlKey @"keythumbnailUrl"
#define imageUrlKey @"keyimageUrl"
#define dataStoragePath @"arrayOfRSSItems"

@implementation RSSItem

+(void)allItemsOnCompletion:(void (^)(NSArray *items, NSError *error))block {
    
    
    ELIRequestOperationManager * operation = [[ELIRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://www.mirror.co.uk/news/"]];
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/rss+xml"];
    [operation GET:@"?service=rss&isMobile=true" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        block([self processRawDataFromDownload:responseObject], nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSArray *rssItems = [RSSItem loadRSSItemsFromDisk];
        if (rssItems) {
            block(rssItems, nil);
        }
        else
        {
            block(nil, error);
        }
    }];
}

+(NSArray*)processRawDataFromDownload:(id)data
{
    NSError *error = nil;
    DDXMLDocument *document = [[DDXMLDocument alloc]initWithData:data options:0 error:&error];
    if (document==nil) {
        return nil;
    }
    NSMutableArray *objects = [[NSMutableArray alloc]init];
    NSArray *array =  [document nodesForXPath:@"//item" error:&error];
    for (DDXMLElement *element in array) {
        RSSItem *elementForXML = [[RSSItem alloc]init];
        elementForXML.title = [[element elementsForName:@"title"][0] stringValue] ;
        elementForXML.link = [NSURL URLWithString:[[element elementsForName:@"link"][0] stringValue] ];
        elementForXML.description = [[element elementsForName:@"description"][0] stringValue];
        elementForXML.thumbnailUrl = [NSURL URLWithString:[[[[element elementsForName:@"media:thumbnail"] firstObject] attributeForName:@"url"] stringValue]];
        elementForXML.imageUrl = [NSURL URLWithString:[[[[element elementsForName:@"media:content"] firstObject] attributeForName:@"url"] stringValue]];
        [objects addObject:elementForXML];
    }
    [RSSItem saveRSSItemsOnDisk:objects];
    return objects;
}

+(NSString*)returnPathOfTheRSSItems{
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = path[0];
    NSString *filePath = [documentDirectory stringByAppendingString:dataStoragePath];
    
    return filePath;
    
}

+(BOOL)saveRSSItemsOnDisk:(NSArray*)rssItems{

    return [NSKeyedArchiver archiveRootObject:rssItems toFile:[RSSItem returnPathOfTheRSSItems]];
    
}

+(id)loadRSSItemsFromDisk{

    return [NSKeyedUnarchiver unarchiveObjectWithFile:[RSSItem returnPathOfTheRSSItems]];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{

    self = [super init];
    if (self) {
        self.title = [aDecoder decodeObjectForKey:titleKey];
        self.link = [aDecoder decodeObjectForKey:linkKey];
        self.description = [aDecoder decodeObjectForKey:descriptionKey];
        self.thumbnailUrl = [aDecoder decodeObjectForKey:thumbnailUrlKey];
        self.imageUrl = [aDecoder decodeObjectForKey:imageUrlKey];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.title forKey:titleKey];
    [aCoder encodeObject:self.link forKey:linkKey];
    [aCoder encodeObject:self.description forKey:descriptionKey];
    [aCoder encodeObject:self.thumbnailUrl forKey:thumbnailUrlKey];
    [aCoder encodeObject:self.imageUrl forKey:imageUrlKey];
}

@end
