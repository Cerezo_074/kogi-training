//
//  RSSItem.h
//  Kogi_Feed_Example_1
//
//  Created by Ernesto Carrion on 5/13/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSItem : NSObject <NSCoding>

@property(nonatomic, strong) NSString * title;
@property(nonatomic, strong) NSString * link;
@property(nonatomic, strong) NSString * description;
@property(nonatomic, strong) NSURL * thumbnailUrl;
@property(nonatomic, strong) NSURL * imageUrl;

+(void)allItemsOnCompletion:(void(^)(NSArray * items, NSError * error))block;

@end
