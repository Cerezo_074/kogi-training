//
//  main.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 26/04/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ELIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ELIAppDelegate class]));
    }
}
