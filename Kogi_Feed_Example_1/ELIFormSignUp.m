//
//  ELIFormSignUp.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 1/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELIFormSignUp.h"
#import "CustomUser.h"

@interface ELIFormSignUp () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *activeField;

@property (nonatomic, weak) IBOutlet UIImageView *topImage;
@property (nonatomic, weak) IBOutlet UIImageView *bottomImage;

@end

@implementation ELIFormSignUp

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Sign Up";
    self.txtUserName.delegate = self;
    
    //[self.username becomeFirstResponder];
    self.txtPassword.delegate = self;
    
    //add an UItapGestureRecogniser for the UIViewController
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    self.topImage.backgroundColor = [UIColor redColor];
    self.bottomImage.backgroundColor = [UIColor redColor];
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    CGRect frameTopImage = self.topImage.frame;
    CGRect frameBottomImage = self.bottomImage.frame;
    [UIView animateKeyframesWithDuration:1.0 delay:1 options:UIViewAnimationCurveEaseOut animations:^{
        
        [self.topImage setFrame:CGRectMake(frameTopImage.origin.x - frameTopImage.size.width, frameTopImage.origin.y , frameTopImage.size.width, frameTopImage.size.height)];
        [self.bottomImage setFrame:CGRectMake(frameBottomImage.origin.x + frameBottomImage.size.width, frameBottomImage.origin.y , frameBottomImage.size.width, frameBottomImage.size.height)];
        
    } completion:^(BOOL finished) {
        [self.topImage removeFromSuperview];
        [self.bottomImage removeFromSuperview];
    }];
    
}

#pragma mark Target Actions

- (IBAction)acceptButton:(id)sender {
    [self validateDataOfTheUser];
}

- (IBAction)cancelButton:(id)sender {
    [self.delegate userHasDecidedToBackward:self];
}


#pragma mark Utils Methods

-(void)validateDataOfTheUser
{
    
    NSString *username = self.txtUserName.text;
    NSString *password = self.txtPassword.text;
    
    if ([username length] > 0 && [password length] > 0)
    {
        CustomUser *temp = [[CustomUser alloc]init];
        temp.username = username;
        temp.password = password;
        //Block
        [CustomUser logInUser:temp onCompletion:^(BOOL state , id error) {
            if (state) {
                [self userLogInSuccessfully];
                [self.delegate userHasDecidedToBackward:self];
            }
            else
            {
                if ([error isKindOfClass:[NSError class]]) {
                    [self sendMessageToUser:[error localizedDescription] title:@"Error"];
                }
                else
                {
                    [self sendMessageToUser:error title:@"Error"];
                }
            }
        }];
        
    }
    else
    {
        [self sendMessageToUser:@"Error the Fields must be filled, check these please" title:@"Error"];
    }
    
}


-(void)userLogInSuccessfully
{
    [self eraseAllTheFields];
    [self sendMessageToUser:@"The User was Autentified" title:@"User Created"];    
}

-(void)eraseAllTheFields
{
    self.txtPassword.text = @"";
    self.txtUserName.text = @"";
}

-(void)sendMessageToUser:(NSString*)message title:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                   }
                   );
}

-(void)dismissKeyboard
{
    for (UIView *view in self.scroll.subviews) {
        if ([view isKindOfClass:[UITextField class]] && [view isFirstResponder]) {
            [view resignFirstResponder];
        }
    }
}

#pragma mark Delegate Methods for UITextFields

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
