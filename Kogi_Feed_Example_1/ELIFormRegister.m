//
//  ELIFormRegister.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 1/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELIFormRegister.h"
#import "CustomUser.h"

#define heightForLabel 36.0f
#define heightForTetxField 30.0f
#define spaceBetweenLabelAndTextField 15.0f
#define spaceBetweenblocks 15.0f
#define widthParent 280.0f

@interface ELIFormRegister ()

@property (nonatomic) BOOL globeIsAlive;
@property (nonatomic) float startPointAtTheRight;
@property (nonatomic) float startPointAtTheLeft;
@property (nonatomic) float heightForGlobe;
@property (nonatomic, strong) AVAudioPlayer *player;

@end

@implementation ELIFormRegister

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    
    //Adjust the Image for the animation
    self.imageAnimation.backgroundColor = [UIColor colorWithRed:256/255.0 green:86/255.0 blue:86/255.0 alpha:1];
    UILabel *text = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 170, 100)];
    text.center = self.imageAnimation.center;
    text.text = @"Welcome!!!";
    text.font = [UIFont fontWithName:@"SnellRoundhand-Black" size:30];
    text.textColor = [UIColor whiteColor];
    [self.imageAnimation addSubview:text];
    self.title = @"Register";
    
    //adjust the focus on the UItexfield of the Username
    //[self.txtUsername becomeFirstResponder];
    
    //adjust the delegates for the UITextfileds this for hide the keyboard when the users touch the return o done button
    self.txtEmail.delegate = self;
    self.txtPassword.delegate = self;
    self.txtPhone.delegate = self;
    self.txtUsername.delegate = self;
    
    //adjust the heigth for all the views
    float mark = 0;
    float spaceA = heightForLabel + spaceBetweenLabelAndTextField;
    float spaceB = heightForTetxField + spaceBetweenblocks;
    
    //adjust the Username block
    self.lblUsername.frame = CGRectMake(0, mark, widthParent, heightForLabel);
    mark += spaceA;
    self.txtUsername.frame = CGRectMake(0, mark, widthParent, heightForTetxField);
    mark += spaceB;
    
    //adjust the Password block
    self.lblPassword.frame = CGRectMake(0, mark, widthParent, heightForLabel);
    mark += spaceA;
    self.txtPassword.frame = CGRectMake(0, mark, widthParent, heightForTetxField);
    mark += spaceB;
    
    //adjust the E-mail block
    self.lblEmail.frame = CGRectMake(0, mark, widthParent, heightForLabel);
    mark += spaceA;
    self.txtEmail.frame = CGRectMake(0, mark, widthParent, heightForTetxField);
    mark += spaceB;
    
    //adjust the Phone block
    self.lblPhone.frame = CGRectMake(0, mark, widthParent, heightForLabel);
    mark += spaceA;
    self.txtPhone.frame = CGRectMake(0, mark, widthParent, heightForTetxField);
    mark += spaceB;
    
    //adjust the Birth Day block
    self.lblBirthDay.frame = CGRectMake(0, mark, widthParent, heightForLabel);
    mark += spaceA;
    self.pickDateOfBirth.frame = CGRectMake(0, mark, widthParent, 162);
    mark += 162+20;
    
    //adjust the button
    self.cancel.frame = CGRectMake((self.scrollView.frame.size.width/2) - (20+89), mark, 89, 30);
    self.accept.frame = CGRectMake((self.scrollView.frame.size.width/2) + 20, mark, 89, 30);
    mark += 30+60;
    
    //Adjust the scrollView
    self.scrollView.contentSize = CGSizeMake(widthParent, mark);
    
    //Adjust the properties for the animations
    [self.scrollView setScrollEnabled:NO];
    self.heightForGlobe = 60;
    self.startPointAtTheRight = self.scrollView.frame.size.width - self.globe.frame.size.width;
    self.startPointAtTheLeft = self.scrollView.frame.origin.x;
    self.imageAnimation.frame = CGRectMake(self.imageAnimation.frame.origin.x,self.imageAnimation.frame.origin.y, self.imageAnimation.frame.size.width, self.scrollView.contentSize.height);
    [self.globe setFrame:CGRectMake(self.globe.frame.origin.x, self.scrollView.frame.size.height, self.globe.frame.size.width, self.globe.frame.size.height)];
    
    //add an UItapGestureRecogniser for the globe
    self.globe.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapOnGlobe = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                action:@selector(tapGlobe)];
    tapOnGlobe.numberOfTapsRequired = 1;
    [self.globe addGestureRecognizer:tapOnGlobe];
    
    //add an UItapGestureRecogniser for the UIViewController
    UITapGestureRecognizer *tapOnView = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                               action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapOnView];
    
    [self setAnimationForBackground];
    
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];

    //Start the animation for the globe
    [self startAnimationForGlobe];
}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    [self.view.layer removeAnimationForKey:@"myAnimations"];
    
}

#pragma mark Target Action Accept Button

- (IBAction)pushAcceptButton:(id)sender {
    [self validateTheFields];
}

- (IBAction)pushCancelButton:(id)sender {
    [self.delegate userHasDecidedToBackward:self];
}

#pragma mark Utils Methods

-(void)validateTheFields
{
    
    NSString *userName = self.txtUsername.text;
    NSString *password = self.txtPassword.text;
    NSString *email = self.txtEmail.text;
    NSString *phoneNumber = self.txtPhone.text;
    NSDateComponents *components = [[NSCalendar currentCalendar]components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.pickDateOfBirth.date];
    NSDate *birthDay = [[NSCalendar currentCalendar]dateFromComponents:components];
    
    if ([userName length] > 0 && [password length] > 0 && [self validateWithEmail:email] && [phoneNumber length] > 0)
    {
        
        CustomUser *temp = [[CustomUser alloc]init];
        temp.username = userName;
        temp.password = password;
        temp.email = email;
        temp.phoneNumber = phoneNumber;
        temp.birthDay = birthDay;
        
        //Block
        [CustomUser createRemoteUser:temp onCompletion:^(BOOL state , id error) {
            if (state) {
                [self userCreatedSuccessfully];
                [self.delegate userHasDecidedToBackward:self];
            }
            else
            {
                if ([error isKindOfClass:[NSError class]]) {
                    [self sendMessageToUser:[error localizedDescription] title:@"Error"];
                }
                else
                {
                    [self sendMessageToUser:error title:@"Error"];
                }
            }
        }];
    }
    else
    {
        [self sendMessageToUser:@"Please Verify that all the Fields are filled or the Email is a wrong address" title:@"Error"];
    }
}

-(void)eraseAllTheFields
{
    self.txtEmail.text = @"";
    self.txtPassword.text = @"";
    self.txtPhone.text = @"";
    self.txtUsername.text = @"";
    self.pickDateOfBirth.date = [NSDate date];
}

-(void)userCreatedSuccessfully
{
    [self eraseAllTheFields];
    [self sendMessageToUser:@"The User Was Created" title:@"User Created"];
    [self.navigationController popViewControllerAnimated:TRUE];
    
}

-(void)sendMessageToUser:(NSString*)message title:(NSString*)title
{
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil]show];
                   });
}

-(BOOL)validateWithEmail:(NSString*)email
{
    if ([email length] == 0) {
        return NO;
    }
    
    //NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *regExPattern = @".*@[a-zA-Z]*\\.[a-zA-z]{2,4}";
    NSRegularExpression *regex = [[NSRegularExpression alloc]initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regex numberOfMatchesInString:email options:0 range:NSMakeRange(0, [email length])];
    
    if (regExMatches == 0) {
        return NO;
    }
    else
    {
        return YES;
    }
    
}

-(void)dismissKeyboard
{
    for (UIView *view in self.scrollView.subviews) {
        if ([view isKindOfClass:[UITextField class]] && [view isFirstResponder]) {
            [view resignFirstResponder];
        }
    }
}

#pragma mark Methods for the Animations

-(void)animationForWelcome{

    [UIView animateKeyframesWithDuration:1.0 delay:1 options:UIViewKeyframeAnimationOptionCalculationModeDiscrete animations:^{
        
        [self.imageAnimation setFrame:CGRectMake(self.imageAnimation.frame.origin.x, self.imageAnimation.frame.origin.y + self.scrollView.contentSize.height, self.imageAnimation.frame.size.width, self.imageAnimation.frame.size.height)];
        NSLog(@"Animation Completed");
        
    } completion:^(BOOL finished) {
        
        [self.imageAnimation removeFromSuperview];
        self.imageAnimation = nil;
        [self.scrollView setScrollEnabled:YES];
        
        [UIView animateKeyframesWithDuration:0.25 delay:0.5 options:0 animations:^{
            self.globe.alpha = 0;
        } completion:^(BOOL finished) {
            [self.globe removeFromSuperview];
        }];

        NSLog(@"Animation Finished");
    }];
    
}

-(void)startAnimationForGlobe{
    
    self.globeIsAlive = YES;
    float newX = self.startPointAtTheRight;
    float newY = self.globe.frame.origin.y - self.heightForGlobe;
    [self moveGlobeAtTheRigthWithNewYPosition:newY newX:newX];
}

-(void)moveGlobeAtTheRigthWithNewYPosition:(float)newY newX:(float)newX{

    if (!self.globeIsAlive) {
        [self animationForWelcome];
        return;
    }
    
    [UIView animateWithDuration:1
                          delay:0
                        options:(UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         
                         [self.globe setFrame:CGRectMake(newX, newY, self.globe.frame.size.width, self.globe.frame.size.height)];
                         
                     } completion:^(BOOL finished) {
                         
                         if ((self.globe.frame.origin.y + self.globe.frame.size.height) <= 0) {
                             self.globeIsAlive = NO;
                         }
                         
                         float newX = self.startPointAtTheLeft;
                         float newY = self.globe.frame.origin.y - self.heightForGlobe;
                         [self moveGlobeAtTheLeftWithNewYPosition:newY newX:newX];

                     }];
}

-(void)moveGlobeAtTheLeftWithNewYPosition:(float)newY newX:(float)newX{
 
    if (!self.globeIsAlive) {
        //
        [self animationForWelcome];
        return;
    }
    
    [UIView animateWithDuration:1
                          delay:0
                        options:(UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionAllowUserInteraction)
                     animations:^{
                         
                         [self.globe setFrame:CGRectMake(newX, newY, self.globe.frame.size.width, self.globe.frame.size.height)];
                         
                     } completion:^(BOOL finished) {
                         
                         if ((self.globe.frame.origin.y + self.globe.frame.size.height) <= 0) {
                             self.globeIsAlive = NO;
                         }
                         
                         float newX = self.startPointAtTheRight;
                         float newY = self.globe.frame.origin.y - self.heightForGlobe;
                         [self moveGlobeAtTheRigthWithNewYPosition:newY newX:newX];
                         
                     }];
}

-(void)setAnimationForBackground{
    
    NSArray *colors = @[[UIColor colorWithRed:240/255.0 green:127/255.0 blue:127/255.0 alpha:1],
                        [UIColor colorWithRed:127/255.0 green:184/255.0 blue:240/255.0 alpha:1],
                        [UIColor colorWithRed:127/255.0 green:240/255.0 blue:127/255.0 alpha:1],
                        [UIColor colorWithRed:184/255.0 green:127/255.0 blue:240/255.0 alpha:1],
                        [UIColor colorWithRed:240/255.0 green:240/255.0 blue:127/255.0 alpha:1]
                        ];
    NSMutableArray *animations = [NSMutableArray new];
    float colorAnimationDuration = 15.0f;
    for (int i = 0; i < colors.count; i++)
    {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
        animation.toValue = (id)[[colors objectAtIndex:i] CGColor];
        animation.duration = colorAnimationDuration;
        animation.beginTime = i * colorAnimationDuration;
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
        
        [animations addObject:animation];
    }
    
    CAAnimationGroup *animationsGroup = [CAAnimationGroup new];
    animationsGroup.duration = colors.count * colorAnimationDuration;
    animationsGroup.repeatCount = HUGE_VALF;
    animationsGroup.animations = animations;
    
    [self.view.layer addAnimation:animationsGroup forKey:@"myAnimations"];
    
}

#pragma mark Delegate Methods for UITextFields

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Methods for Touch

-(void)tapGlobe{

    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *pathOfSmashGlobe = [[NSBundle mainBundle] pathForResource:@"globe_smash" ofType:@"mp3"];
        NSURL *smashGlobe = [NSURL fileURLWithPath:pathOfSmashGlobe];
        NSError *error = nil;

        self.player = [[AVAudioPlayer alloc]initWithContentsOfURL:smashGlobe error:&error];
       
        if (error) {
            
            NSLog(@"Error: %@",[error localizedDescription]);
        }
        else
        {
            [self.player play];
            
            [UIView animateKeyframesWithDuration:0.25 delay:0.5 options:0 animations:^{
                self.globe.alpha = 0;
            } completion:^(BOOL finished) {
                [self.globe removeFromSuperview];
            }];
        }
    });
}

@end
