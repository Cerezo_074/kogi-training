//
//  ELIFormSignUp.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 1/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SBFormViewController.h>
#import "ELISignUpProtocol.h"

@interface ELIFormSignUp : SBFormViewController

@property (nonatomic, weak) IBOutlet UILabel *lblUsername;
@property (nonatomic, weak) IBOutlet UILabel *lblPassword;
@property (nonatomic, weak) IBOutlet UITextField *txtUserName;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;
@property (nonatomic, weak) IBOutlet UIScrollView *scroll;
@property (nonatomic, weak) id<ELISignUpProtocol> delegate;

@end
