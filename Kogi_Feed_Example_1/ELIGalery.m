//
//  ELIGalery.m
//  Kogi_Feed_Example_1
//
//  Created by Adriana on 31/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELIGalery.h"

@implementation ELIGalery

-(NSString*)description{

    return [NSString stringWithFormat:@"Titile: %@\n Images:\n %@",self.title,self.gallery];
    
}

@end
