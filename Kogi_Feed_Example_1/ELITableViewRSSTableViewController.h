//
//  ELITableViewRSSTableViewController.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 26/04/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ELITableViewRSSTableViewController : UITableViewController

@property(nonatomic, strong) NSArray * items;

@end
