//
//  ELIRegisterProtocol.h
//  Kogi_Feed_Example_1
//
//  Created by Adriana on 24/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ELIRegisterProtocol <NSObject>

-(void)userHasDecidedToBackward:(id)presentingController;

@end
