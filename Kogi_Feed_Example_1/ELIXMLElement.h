//
//  ELIXMLElement.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 26/04/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELIXMLElement : NSObject

@property(nonatomic, strong) NSString *title;
@property(nonatomic, strong) NSURL *link;
@property(nonatomic, strong) NSString *descriptionOfElement;
@property(nonatomic, strong) NSURL *urlOfSmallPhoto;
@property(nonatomic, strong) NSURL *urlOfBigPhoto;

@end
