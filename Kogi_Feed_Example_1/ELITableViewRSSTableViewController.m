//
//  ELITableViewRSSTableViewController.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 26/04/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELITableViewRSSTableViewController.h"
#import "ELICustomCell.h"
#import "ELIFormSignUp.h"
#import "PageViewControllerForRSSItems.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "RSSItem.h"

NSString *const customCellIdentifier = @"customCell";

@implementation ELITableViewRSSTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = @"Kogi Feed";
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //adjust the Custom cells
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"loadingCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ELICustomCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:customCellIdentifier];
    
    [RSSItem allItemsOnCompletion:^(NSArray *items, NSError *error) {
        
        if (!error) {
            
            self.items = items;
            [self.tableView reloadData];
            
        } else {
            
            [[[UIAlertView alloc]initWithTitle:@"Error"
                                       message:error.localizedDescription
                                      delegate:nil
                             cancelButtonTitle:nil
                             otherButtonTitles:@"Ok!", nil] show];
        }
        
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.items == nil) {
        return 1;
    }
    return [self.items count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.items == nil) {
        return 25;
    }
    RSSItem * item = self.items[indexPath.row];
    CGRect size = [item.title boundingRectWithSize:CGSizeMake(206, MAXFLOAT)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName:[UIFont fontWithName:@"ChalkboardSE-Light" size:16]}
                                           context:nil];
    if (size.size.height < 106) {
        return 106;
    }
    return size.size.height+10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.items == nil) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadingCell"];
        cell.textLabel.text = @"Loading...";
        return cell;
    }
    
    ELICustomCell *cell = [tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
    RSSItem * item = self.items[indexPath.row];
    cell.titleOfTheNotice.text = item.title;
    
    //Download the image on asynchronous mode
    if (item.thumbnailUrl) {
        
        [cell.ImageOfTheNotice setImageWithURL:item.thumbnailUrl placeholderImage:[UIImage imageNamed:@"swordsman"]];
        
    } else {
        
        cell.ImageOfTheNotice.image = nil;
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Create the notice selected for the user
    PageViewControllerForRSSItems *noticesOnPageViewController = [[PageViewControllerForRSSItems alloc]initWithModel:self.items IndexForCurrentRSSItem:indexPath.row];
    [self.navigationController pushViewController:noticesOnPageViewController animated:YES];
}

@end