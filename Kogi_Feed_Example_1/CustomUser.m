//
//  CustomUser.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 5/15/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "CustomUser.h"
#import "ELIRequestOperationManager.h"

#define usernameKey @"keyUser"
#define passwordKey @"keyPassword"
#define emailKey @"keyEmail"
#define phonenumberKey @"keyPhoneNumber"
#define birthdayKey @"keyBirthDay"
#define urlWebService @"http://elipruebas.site40.net/webservice/start.php"

static CustomUser *currentUser = nil;
static NSString * userStoragePath = @"/user.obj";
static dispatch_once_t onceToken;

@implementation CustomUser

+(CustomUser*)currentUser{
    dispatch_once(&onceToken, ^{
        currentUser = [CustomUser loadUserFromDiskWithPath:userStoragePath];
        if (!currentUser) {
            currentUser = [[CustomUser alloc] init];
        }
    });
    
    return currentUser;
    
}

+(void)setCurrentUser:(CustomUser*)user {
    
    [CustomUser currentUser];
    currentUser = nil;
    currentUser = user;
    
}

+(CustomUser*)loadUserFromDiskWithPath:(NSString*)userStoragePath{
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = path[0];
    NSString *filePath = [documentDirectory stringByAppendingString:userStoragePath];
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    
}

+(BOOL)saveUserToDisk:(CustomUser*)user userStoragePath:(NSString*)userStoragePath
{
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = path[0];
    NSString *filePath = [documentDirectory stringByAppendingString:userStoragePath];
    
    return [NSKeyedArchiver archiveRootObject:user toFile:filePath];
    
}

+(BOOL)deleteUserFromDiskAndMemory{
    
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = path[0];
    NSString *filePath = [documentDirectory stringByAppendingString:userStoragePath];
    NSError *error = nil;
    BOOL complete = NO;
    
    if ([[NSFileManager defaultManager] removeItemAtPath:filePath error:&error]) {
        if (!error) {
            complete = YES;
            currentUser = nil;
        }
    }
    
    return complete;
    
}

+(void)createRemoteUser:(CustomUser*)user onCompletion:(void(^)(BOOL state, id error))block{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *params = @{@"fn":@"insertUser",@"type":@"json",@"username":user.username,@"password":user.password,@"email":user.email,@"phonenumber":user.phoneNumber,@"birthday":user.birthDay};
    [manager POST:urlWebService parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *ditc = responseObject;
        NSNumber *result = [ditc objectForKey:@"result"];
        
        if([result  isEqual: @(1)]){
            [CustomUser setCurrentUser:user];
            [[CustomUser currentUser] saveUserState];
            block([[CustomUser currentUser] saveUserState], nil);
        }
        else
        {
            block(NO,@"The User Exist, Please Choose another Username");
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(NO,error);
    }];
    
}

+(void)logInUser:(CustomUser*)user onCompletion:(void(^)(BOOL state, id error))block{
    
    AFHTTPRequestOperationManager *manager = [ELIRequestOperationManager manager];
    NSDictionary *params = @{@"fn":@"logIn",@"type":@"json",@"username":user.username,@"password":user.password};
    [manager GET:urlWebService parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *ditc = responseObject;
        if ([[ditc allKeys]count]>3) {
            user.email = [ditc objectForKey:@"email"];
            user.phoneNumber = [ditc objectForKey:@"phonenumber"];
            user.birthDay = [ditc objectForKey:@"birthday"];
            [CustomUser setCurrentUser:user];
            block([[CustomUser currentUser]saveUserState],nil);
            
        }
        else
        {
            if ([currentUser.username isEqualToString:user.username] && [currentUser.password isEqualToString:user.password]) {
                block([[CustomUser currentUser]saveUserState],nil);
            }
            else
            {
                block(NO,@"Password or Username were invalid");
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        block(NO,error);
    }];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super init];
    if(self){
        self.username = [aDecoder decodeObjectForKey:usernameKey];
        self.password = [aDecoder decodeObjectForKey:passwordKey];
        self.email = [aDecoder decodeObjectForKey:emailKey];
        self.phoneNumber = [aDecoder decodeObjectForKey:phonenumberKey];
        self.birthDay = [aDecoder decodeObjectForKey:birthdayKey];
    }
    
    return self;
    
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.username forKey:usernameKey];
    [aCoder encodeObject:self.password forKey:passwordKey];
    [aCoder encodeObject:self.email forKey:emailKey];
    [aCoder encodeObject:self.phoneNumber forKey:phonenumberKey];
    [aCoder encodeObject:self.birthDay forKey:birthdayKey];
    
}

-(BOOL)saveUserState{
    
    return  [CustomUser saveUserToDisk:self userStoragePath:userStoragePath];
    
}


@end
