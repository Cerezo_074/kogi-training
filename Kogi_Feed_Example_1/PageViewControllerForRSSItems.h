//
//  PageViewControllerForRSSItems.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 23/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUser.h"

@interface PageViewControllerForRSSItems : UIViewController

@property(nonatomic, strong) NSArray *model;
@property(nonatomic, strong) UIPageViewController *pageController;
@property(nonatomic, weak) IBOutlet UIView* tempView;
@property (nonatomic, strong) UIBarButtonItem *userButton;
@property (nonatomic, weak) CustomUser* user;

-(id)initWithModel:(NSArray*)model IndexForCurrentRSSItem:(NSInteger)index;

-(void)updateCurrentUserStateOnCorner;

- (void)startTheProcessForLogInWithUserName:(NSString*)username password:(NSString*)password;

@end
