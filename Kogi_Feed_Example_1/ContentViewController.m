//
//  ContentViewController.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 2/06/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ContentViewController.h"
#import "ELIGalery.h"
#import "ELIImageOfGalery.h"
#import "CustomCollectionViewCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#define cellIdentifier @"cellIdentifier"

@interface ContentViewController ()

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) NSArray *dataArray;
@property(nonatomic) int currentCell;
@property(nonatomic) int currentSection;

@property(nonatomic) int currentIndex;

@end

@implementation ContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithArrayOfGalleries:(NSArray*)arrayOfTheURLs{

    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.dataArray = arrayOfTheURLs;
    }
    
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    //Adjust the Bounds for the SuperView
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]){
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeRight)];
    swipeRight.delegate = self;
    swipeRight.numberOfTouchesRequired = 0;
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.collectionView addGestureRecognizer:swipeRight];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipeLeft)];
    swipeLeft.delegate = self;
    swipeLeft.numberOfTouchesRequired = 0;
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.collectionView addGestureRecognizer:swipeLeft];
    
    [self setUpCollectionView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Utils Methods

-(void)setUpCollectionView{

    [self.collectionView registerClass:[CustomCollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
    
    //Adjust the FlowLayout For The Transitions Between the Items
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [flowLayout setMinimumInteritemSpacing:0.0f];
    [flowLayout setMinimumLineSpacing:0.0f];
    [self.collectionView setPagingEnabled:YES];
    [self.collectionView setCollectionViewLayout:flowLayout];
    
}

#pragma mark Delegate Methods For The Collection View


#pragma mark DataSource Methods For The Collection View

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    ELIGalery *gallery = [self.dataArray objectAtIndex:section];
    return [gallery.gallery count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    CustomCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    self.currentCell = indexPath.row;
    self.currentSection = indexPath.section;
    
    ELIGalery *tempGallery =  (ELIGalery*)[self.dataArray objectAtIndex:indexPath.section];
    ELIImageOfGalery *tempImageOfGallery = tempGallery.gallery [indexPath.row];
    
    //Adjust the Properties For the cell
    cell.url = tempImageOfGallery.url;
    cell.caption.numberOfLines = 0;
    
    //We Calculate the Size Ideal  For the UI Label
    CGRect oldRect = cell.caption.frame;
    CGRect expectLabelSize = [tempImageOfGallery.caption boundingRectWithSize:CGSizeMake(320, MAXFLOAT)
                                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                                   attributes:@{NSFontAttributeName:[UIFont fontWithName:@"SnellRoundhand" size:23]}
                                                                      context:nil];
    oldRect.size.height = expectLabelSize.size.height;
    cell.caption.frame = oldRect;
    cell.caption.text = tempImageOfGallery.caption;
    cell.scrollView.contentSize = CGSizeMake(cell.frame.size.width, (20+cell.image.frame.size.height)+(8+cell.caption.frame.size.height+20));
    
    //Adjust the Params for the Donwload of the Image
    UIImage *tempImage = [UIImage imageNamed:@"wait_please"];
    NSURLRequest *request = [NSURLRequest requestWithURL:tempImageOfGallery.url];
    [cell.image setImageWithURLRequest:request placeholderImage:tempImage success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        [cell.image setImage:image];
        [cell.image setContentMode:UIViewContentModeScaleAspectFit];
        
        //NSLog(@"Size For the CollectionView Size (%f,%f)",self.collectionView.frame.size.width,self.collectionView.frame.size.height);
        //NSLog(@"Cell Origin (%f,%f) - Size (%f,%f)",cell.frame.origin.x,cell.frame.origin.y,cell.frame.size.width,cell.frame.size.height);
        //NSLog(@"ContentOffset For CollectionView (%f,%f)",self.collectionView.contentOffset.x,self.collectionView.contentOffset.y);
        //NSLog(@"Image Origin (%f,%f) Size (%f,%f)",cell.image.frame.origin.x,cell.image.frame.origin.y,cell.image.frame.size.width,cell.image.frame.size.height);
        //NSLog(@"Label Origin (%f,%f) Size (%f,%f)",cell.caption.frame.origin.x,cell.caption.frame.origin.y,cell.caption.frame.size.width,cell.caption.frame.size.height);
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        NSLog(@"Error");
    }];
    
    return cell;
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{

    return [self.dataArray count];
    
}

#pragma mark Methods For The Delegate UIControllerFlowLayout

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    NSLog(@"Size For the CollectionView Size (%f,%f)",self.collectionView.frame.size.width,self.collectionView.frame.size.height);
    return self.collectionView.frame.size;

}

#pragma mark Methods For The Rotation

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self.collectionView setAlpha:0.0f];
    CGPoint currentOffset = [self.collectionView contentOffset];
    self.currentIndex = currentOffset.x/self.collectionView.frame.size.width;
    
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{

    CGSize currentSize = self.collectionView.bounds.size;
    float offset = self.currentIndex * currentSize.width;
    [self.collectionView setContentOffset:CGPointMake(offset, 0)];
    [UIView animateWithDuration:0.125f animations:^{
        [self.collectionView setAlpha:1.0f];
    }];

}

#pragma mark UIScrollView Desacelerating 

-(void)didSwipeRight{

    NSInteger numberOfTheLastOfSection = [self.dataArray count]-1;
    NSInteger numberOfElementsOnLastSection = [[self.dataArray objectAtIndex:numberOfTheLastOfSection] count]-1;
    
    if (numberOfTheLastOfSection == self.currentSection) {
        
        if (numberOfElementsOnLastSection == self.currentCell) {
            NSLog(@"The User Want To go out");
        }
        
    }
    
}

-(void)didSwipeLeft{
    
    
    
}

@end
