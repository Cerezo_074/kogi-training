//
//  ELICustomAlertViewViewController.h
//  Kogi_Feed_Example_1
//
//  Created by Adriana on 24/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ELICustomAlertViewViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end
