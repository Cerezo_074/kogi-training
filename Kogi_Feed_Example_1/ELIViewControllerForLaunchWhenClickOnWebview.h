//
//  ELIViewControllerForLaunchWhenClickOnWebview.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 2/06/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ELIViewControllerForLaunchWhenClickOnWebview : UIViewController

@property(nonatomic, weak) IBOutlet UIWebView *webView;
@property(nonatomic, strong) NSURL *url;
@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *indicator;

-(id)initWithUrl:(NSURL*)url;

@end
