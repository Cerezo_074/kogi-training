//
//  ELIRequestOperationManager.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 5/20/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface ELIRequestOperationManager : AFHTTPRequestOperationManager

@end
