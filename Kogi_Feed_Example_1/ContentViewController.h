//
//  ContentViewController.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 2/06/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate>

-(id)initWithArrayOfGalleries:(NSArray*)arrayOfTheURLs;

@end
