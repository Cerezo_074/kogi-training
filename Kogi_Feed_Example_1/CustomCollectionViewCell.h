//
//  CustomCollectionViewCell.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 2/06/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell

@property(nonatomic, weak) IBOutlet UIImageView *image;
@property(nonatomic, weak) IBOutlet UILabel *caption;
@property(nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property(nonatomic, strong) NSURL *url;

@end
