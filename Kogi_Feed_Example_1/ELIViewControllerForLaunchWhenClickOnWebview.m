//
//  ELIViewControllerForLaunchWhenClickOnWebview.m
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 2/06/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELIViewControllerForLaunchWhenClickOnWebview.h"

@interface ELIViewControllerForLaunchWhenClickOnWebview ()<UIWebViewDelegate>

@end

@implementation ELIViewControllerForLaunchWhenClickOnWebview

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithUrl:(NSURL*)url{

    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        self.url = url;
    }
    
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.webView.delegate = self;
    [self.indicator startAnimating];
    NSURLRequest *request = [NSURLRequest requestWithURL:self.url];
    [self.webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Methods For Webview Delegate

-(void)webViewDidFinishLoad:(UIWebView *)webView{

    [self.indicator stopAnimating];
    
}

@end
