//
//  ELICustomCell.h
//  Kogi_Feed_Example_1
//
//  Created by Eli Pacheco Hoyos on 26/04/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ELICustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleOfTheNotice;
@property (weak, nonatomic) IBOutlet UIImageView *ImageOfTheNotice;

@end
