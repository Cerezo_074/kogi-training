//
//  ELIImageOfGalery.m
//  Kogi_Feed_Example_1
//
//  Created by Adriana on 31/05/14.
//  Copyright (c) 2014 Eli Pacheco Hoyos. All rights reserved.
//

#import "ELIImageOfGalery.h"

@implementation ELIImageOfGalery

-(NSString*)description{

    return [NSString stringWithFormat:@"URL: %@\n Caption: %@",self.url,self.caption];
    
}

@end
